#!/bin/bash
files=encipheredSamples/*

mkdir decTests

for file in $files
do
    #echo ${file:18:-11}
    python2 static/syms2count.py $file
    mkdir ${file:18:-11}
    mv cipher.1gram ${file:18:-11}
    mv cipher.2gram ${file:18:-11}
    mv cipher.3gram ${file:18:-11}
    mv cipher.4gram ${file:18:-11}
    cp static/flagfile ${file:18:-11}
    cp static/run.sh ${file:18:-11}
    cp static/nyt4.arpa ${file:18:-11}
    cp static/solveUnravel.py ${file:18:-11}
    cp $file ${file:18:-11}
    
    mv ${file:18:-11} decTests
done