#!/bin/bash
files=decTests/*

for file in $files
do
    cd $file
    chmod +x run.sh
    ./run.sh
    cd ../..

done