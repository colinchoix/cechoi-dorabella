import sys
import gzip

key = {}
mapping = gzip.open(sys.argv[2]).read().decode("utf-8") 
cipher = open(sys.argv[1]).read()

for x in mapping.split("\n")[:-1]:
    t = x.split("\t")
    key[t[0].lower()] = t[1].lower()

plaintext = "".join([key[x.lower()] for x in cipher])
print(plaintext)