#!/usr/bin/perl -I/home/cechoi/Desktop/Dorabella Cipher/nuhn-solver
# Re-implementation of "Beam Search for Solving Substitution Ciphers"
# by Malte Nuhn, Julian Schamper, and Hermann Ney
# Re-implemented by Bradley Hauer (bmhauer@ualberta.ca)

use warnings;
use strict;
use Getopt::Long;
use ReadLM;

$| = 1;

my $countsfile = '';
my $intfile = '';
my $nkeep = 100;
my $word = 0;
my $use_iterative_beam_search = 0;
my @vocab_inc = (0,1000,10000);
#my @vocab_inc = (0,10,20,100,1000,10000);
my $verbose = 0;
GetOptions(
  "c=s"  => \$countsfile,
  "d=s"  => \$intfile,
  "b=i"  => \$nkeep,
  "w"    => \$word,
  "i"    => \$use_iterative_beam_search,
  "v"    => \$verbose,
)
|| die "CLI argument error!\n";

print "Running program.\n\n" if $verbose;

my @counts = ReadLM::read_lm($countsfile);
$counts[1]->{'?'} = 1;
my ($order,@lambda) = ReadLM::read_interpolation_file($intfile);

if ($verbose) {
  print "Counts file: $countsfile\n";
  print "Interpolation file: $intfile\n";
  print "\tOrder: $order\n";
  print "\tLambda: [@lambda[1..@lambda-1]]\n";
  print "Beam size: $nkeep\n";
  print "Word cipher? $word\n";
  print "Iterative beam search? $use_iterative_beam_search\n";
  print "\tVocabulary increments: [@vocab_inc]\n" if $use_iterative_beam_search;
  print "\n\n";
}

my $tokens = 0;
foreach my $type (keys %{$counts[1]}) {
  $tokens += $counts[1]->{$type};
}

my @Ve = sort {$counts[1]->{$b} <=> $counts[1]->{$a}} keys %{$counts[1]};
for (my $i = 0; $i < @Ve; $i++) {
  if ($Ve[$i] eq '_' || $Ve[$i] eq '?' || $Ve[$i] eq '$') {
    splice(@Ve,$i,1);
  }
}
my $nmax = 1;

my $ctext = ''; # declare the ctext in program-wide scope
my @tokens = (); # declare the list of tokens in program-wide scope.
my %ngramcount = ();

if (!$word) {
  while (<>) {
    chomp;
    $ctext = $_;
    $ctext =~ s/\s+/_/g;
    $ctext = join(' ',split(//,$ctext));
    
    print "Solving '$ctext'\n" if $verbose;
    my $phi = solve($ctext, \@Ve, $nmax, $nkeep, $use_iterative_beam_search);
    if ($verbose) {
      print "\nSolution:\n";
      printkey($phi->[0]);
      print "\n";
      print "$ctext\n";
      print decipher($phi->[0],$ctext), "\n";
      print "\n";
      print accuracy($phi->[0],$ctext), "\n";
    }
    else {
      my $deciph = decipher($phi->[0],$ctext);
      $deciph =~ s/\s+//g;
      $deciph =~ s/_/ /g;
      print "$deciph\t", accuracy($phi->[0],$ctext);
    }
  }
}
else {
  $ctext = '';
  while (<>) {
    s/^\s*//g;
    s/\s+/ /g;
    $ctext .= $_;
  }
  $ctext =~ s/^\s*//g;
  $ctext =~ s/\s*$//g;
  @tokens = split /\s+/, $ctext;
  for (1..$order-1) {
    unshift @tokens, '$';
    push @tokens, '$';
  }
  for (my $i = 0; $i < @tokens-$order; $i++) {
    my $ngram = join(' ', @tokens[$i..($i+$order-1)]);
    no warnings 'uninitialized';
    $ngramcount{$ngram}++;
  }
  
  print "Solving word substitution cipher (". substr($ctext,0,50) .")...\n" if $verbose;
  my $phi = solve(\@Ve, $nmax, $nkeep, $use_iterative_beam_search);
  if ($verbose) {
    print "\nSolution:\n";
    printkey($phi->[0]);
    print "\n";
    print "$ctext\n";
    print decipher($phi->[0],$ctext), "\n";
    print "\n";
    print accuracy($phi->[0],$ctext), "\n";
  }
  else {
    print decipher($phi->[0],$ctext), "\t";
    print accuracy($phi->[0],$ctext) if $verbose; 
  }
}




sub solve {
  my ($Ve, $nmax, $nkeep, $use_iterative_beam_search) = @_;
  my @Vf = get_ext_order($ctext);
  my @ext_order = @Vf;
  my $phi;
  my $phiinit = [{'_' => '_', '$' => '$'},-inf];
  if (!$use_iterative_beam_search) {
    print "Using regular beam search.\n" if $verbose;
    $phi = beam_search($Ve, \@Vf, $order, \@ext_order, $nmax, $nkeep, $phiinit);
  }
  else {
    print "Using iterative beam search.\n" if $verbose;
    $phi = iterative_beam_search($Ve, \@Vf, $order, \@ext_order, $nmax, $nkeep, $phiinit);
  }
  return $phi;
}



sub iterative_beam_search {
  my ($Ve, $Vf, $order, $ext_order, $nmax, $nkeep, $phiinit) = @_;
  
  my $phi = [];
  copy($phiinit,$phi);
  print "\n" if $verbose;
  for (my $i = 1; $i < @vocab_inc; $i++) {
    print "START PHASE $i\n" if $verbose;
    printkey1line($phi->[0]) if $verbose;
    
    my $ilb = $vocab_inc[$i-1]; # index lower bound
    my $iub = $vocab_inc[$i]-1; # index upper bound
    print "Vocab limits $ilb..$iub\n" if $verbose;
    my $stop = 0;
    if ($iub >= @$Ve) {
      $iub = @$Ve-1;
      $stop = 1;
    }
    if ($iub >= @$Vf) {
      $iub = @$Vf-1;
      $stop = 1;
    }
    if ($iub >= @$ext_order) {
      $iub = @$ext_order-1;
      $stop = 1;
    }
    
    my $Ve1 = [@$Ve[$ilb..$iub]];
    my $Vf1 = [@$Vf[$ilb..$iub]];
    my $eo1 = [@$ext_order[$ilb..$iub]];
    my $phi1 = beam_search($Ve1, $Vf1, $order, $eo1, $nmax, $nkeep, $phi);
    copy($phi1,$phi);
    
    print "END PHASE $i\n" if $verbose;
    printkey1line($phi->[0]) if $verbose;
    print "ACCURACY AFTER PHASE $i:\t",accuracy($phi->[0]) if $verbose;
    print "\n" if $verbose;
    last if $stop;
  }
  return $phi;
}



sub beam_search {
  my ($Ve, $Vf, $order, $ext_order, $nmax, $nkeep, $phiinit) = @_;

  my $Hs = [];
  my $Ht = [];
  my $cardinality = 0;
  my $phizero = [];
  copy($phiinit,$phizero);
  push @$Hs, $phizero;

  while ($cardinality < @$Vf) {
    my $f = $ext_order->[$cardinality];
    print "Current cardinality is $cardinality/".scalar(@$Vf).", adding '$f'.\n" if $verbose;
    foreach my $phi (@$Hs) {
      foreach my $e (@$Ve) {
        my $phiprime = [];
        copy($phi,$phiprime);
        $phiprime->[0]->{$f} = $e;
        if (ext_limits($phiprime, $nmax)) {
          if ($word) {
            $phiprime->[1] = score($phiprime->[0]);
          }
          else {
            $phiprime->[1] = score_linear($phiprime->[0]);
          }
          push @$Ht, $phiprime;
        } #endif
      } #endforeach
    } #endforeach

    prune($Ht, $nkeep);
    $cardinality++;
    @$Hs = @$Ht;
    @$Ht = ();
    print "Best key:\t" if $verbose;
    printkey1line($Hs->[0]->[0]) if $verbose;
    print "ACCURACY:\t",accuracy($Hs->[0]->[0]) if $verbose;
    print `date` if $verbose;
    print "\n" if $verbose;
  } #endwhile
  
  my ($max, $argmax) = (-inf, -1);
  for (my $i = 0; $i < @$Hs; $i++) {
    if ($Hs->[$i]->[1] > $max) {
      $max = $Hs->[$i]->[1];
      $argmax = $i;
    }
  }
  return $Hs->[$argmax];
} #endsub



# Each v_e can be used (i.e. mapped to) at most $nmax times.
# Return 1 if $phi satisfies this requirement, 0 if it does not.
sub ext_limits {
  my ($phi, $nmax) = @_;
  my %mappings_count = ();
  foreach my $f (sort keys %{$phi->[0]}) {
    my $e = $phi->[0]->{$f};
    $mappings_count{$e} = $mappings_count{$e} ? $mappings_count{$e}+1 : 1;
    return 0 if $mappings_count{$e} > $nmax;
  } #endforeach
  return 1;
}


sub score {
  my ($key) = @_;
  
  my $score = 0;
  foreach my $f (keys %ngramcount) {
    my @f = split /\s+/, $f;
    # Decipher $f
    my @ef = ();
    foreach my $fprime (@f) {
      push @ef, $key->{$fprime} ? $key->{$fprime} : '?';
    }
    next if $ef[-1] eq '?';

    my $localscore = 0;
    $localscore += ($lambda[1] * ($counts[1]->{$ef[-1]}) / ($tokens));
    foreach my $n (2..$order) {
      my @e = @ef[scalar(@ef)-$n..scalar(@ef)-1];
      die "Error 1!" unless @e == $n;
        
      my $e = join(' ', @e);
      my $econdition = join(' ', @e[0..scalar(@e)-2]);
      
      next unless ($counts[$order]->{"$e"} && $counts[$order-1]->{"$econdition"});
    
      $localscore += ($lambda[$n] * ($counts[$order]->{$e}) / ($counts[$order-1]->{$econdition}));
    }
    $score += log($localscore) * $ngramcount{$f};
  }
  
  return $score;
}



sub score_linear {
  my ($key) = @_;
  #my @tokens = split /\s+/, $ctext;
  #for (1..$order-1) {
  #  unshift @tokens, '$';
  #  push @tokens, '$';
  #}
  
  my $score = 0;
  for (my $i = $order-1; $i < @tokens; $i++) {
    my @f = @tokens[$i-$order+1..$i];
    my $f = join(' ', @f);
    # Check if $f is newly fixed.
    
    # Decipher $f
    my @ef = ();
    foreach my $fprime (@f) {
      push @ef, $key->{$fprime} ? $key->{$fprime} : '?';
    }

    my $localscore = 0;
    $localscore += ($lambda[1] * ($counts[1]->{$ef[-1]}) / ($tokens));
    next if $ef[-1] eq '?';
    foreach my $n (2..$order) {
      my @e = @ef[scalar(@ef)-$n..scalar(@ef)-1];
      die "Error 1!" unless @e == $n;
        
      my $e = join(' ', @e);
      my $econdition = join(' ', @e[0..scalar(@e)-2]);
      
      next unless ($counts[$order]->{"$e"} && $counts[$order-1]->{"$econdition"});
    
      $localscore += ($lambda[$n] * ($counts[$order]->{$e}) / ($counts[$order-1]->{$econdition}));
    }
    $score += log($localscore);
  }
  
  return $score;
}


# Determine the order that the symbols in $ctext will be deciphered.
# For all word substitution experiments, this is simply ordering the symbols
# by frequency, highest frequency first.
sub get_ext_order {
  my ($ctext) = @_;
  my @tokens = split /\s+/, $ctext;
  my %count = ();
  foreach my $f (@tokens) {
    $count{$f} = $count{$f} ? $count{$f}+1 : 1;
  }
  delete($count{'_'}) if $count{'_'};
  delete($count{'$'}) if $count{'$'};
  return sort {$count{$b} <=> $count{$a}} keys %count;
}



# We can keep only $nkeep partial cipher functions for each iteration.
# Sort the pcfs in $Ht by score (highest first), and keep the best $nkeep.
sub prune {
  my ($Ht, $nkeep) = @_;
  @$Ht = sort {$b->[1] <=> $a->[1]} @$Ht;
  if (@$Ht > $nkeep) {
    @$Ht = @$Ht[0..$nkeep-1];
  }
}



# Given two partial cipher functions, copy the first into the second.
sub copy {
  my ($src, $dst) = @_;
  foreach my $key (keys %{$src->[0]}) {
    $dst->[0]->{$key} = $src->[0]->{$key};
  }
  $dst->[1] = $src->[1];
}

# Print a key, one element per line, for easy examination.
sub printkey {
  my ($key) = @_;
  foreach my $f (sort keys %{$key}) {
    print "$f  ->  $key->{$f}\n" if $verbose
  }
}

# Print a key, all on one line, for easy examination.
sub printkey1line {
  my ($key) = @_;
  foreach my $f (sort keys %{$key}) {
    print "($f,$key->{$f}) "  if $verbose
  }
  print "\n" if $verbose;
}

# Given a key and a ciphertext, decipher the ciphertext with the key.
sub decipher {
  my ($key) = @_;
  my @tokens = split /\s+/, $ctext;
  my @dtokens = ();
  foreach my $f (@tokens) {
    my $e = $key->{$f};
    push @dtokens, $e ? $e : '?';
  } #endforeach
  return join ' ', @dtokens;
}

# Given a key, determine its accuracy
sub accuracy {
  my ($key) = @_;
  my @ctokens = split /\s+/, $ctext;
  my $correct = 0;
  my $total = 0;
  
  foreach my $f (@ctokens) {
    $total++;
    next unless $key->{$f};
    if ($key->{$f} eq lc($f)) {
      $correct++;
    }
  } #endforeach
  
  my $match = 0;
  my $keys = 0;
  foreach my $k (keys %$key) {
    next if $k eq '$';
    $keys++;
    if (lc($k) eq $key->{$k}) {
      $match++;
    }
  }
  
  return sprintf("$correct/$total\t%1.3f\t$match/$keys\t%1.3f\n", $correct/$total, $match/$keys)  if $verbose;
}
