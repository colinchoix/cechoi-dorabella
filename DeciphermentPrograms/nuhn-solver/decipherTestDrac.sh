#!/bin/bash


files=encodedSamples/D*

for file in $files
do
    #cat $file | python3 encodeMASC.py > encodedSamples/${file:12}.out
    #echo ${file:15}
    #perl masc_solver_mcts.pl -n $file > decipherments/${file:15}.dec
    cat $file | python3 split.py | perl sub-cipher-solver-nuhn_etal-2013.pl -c lmtrain_nyt_word.c3lm -d lmtrain_nyt_word.c3di -b 128 -w -i | python3 join.py > decipherments/${file:15}.dec
done