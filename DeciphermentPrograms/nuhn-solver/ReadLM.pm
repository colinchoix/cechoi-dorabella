use warnings;
use strict;

package ReadLM;

sub read_lm {
  my ($file) = @_;
  
  my @counts = ();
  open LM, '<', $file || die $!;
  while (<LM>) {
    chomp;
    my ($n, $ngram, $count) = split /\t+/;
    next unless $n && $ngram && $count;
    $counts[$n] = {} unless $counts[$n];    
    $counts[$n]->{$ngram} = $count;
  }
  close LM || die $!;
  
  return @counts;
}

sub read_interpolation_file {
  my ($file) = @_;  

  my $order = -1;
  my @lambda = (0);
  my @counts = ();
  open LM, '<', $file || die $!;
  while (<LM>) {
    chomp;
    my @tokens = split /\t+/;
    if (@tokens == 1) {
      $order = $tokens[0];
    }
    elsif (@tokens == 2) {
      my ($l,$c) = @tokens;
      $lambda[$l] = $c
    }
    #elsif (@tokens == 3) {
    #  my ($n, $ngram, $count) = @tokens;
    #  next unless $n && $ngram && $count;
    #  $counts[$n] = {} unless $counts[$n];    
    #  $counts[$n]->{$ngram} = $count;
    #}
  }
  close LM || die $!;
  
  return ($order, @lambda);
}

1;
