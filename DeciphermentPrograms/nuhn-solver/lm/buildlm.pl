#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long;

my $char = 0;
my $order = 2;
GetOptions(
  "c"    => \$char,
  "o=i"  => \$order,
)
|| die "CLI argument error!\n";

my @counts = ();
foreach my $n (1..$order) {
  $counts[$n] = {};
}

while (<>) {
  chomp;
  
  my @tokens;
  if ($char) {
    s/\s+/_/g;
    @tokens = split //;
  }
  else {
    @tokens = split /\s+/;
  }
  
  foreach my $n (1..$order-1) {
    unshift @tokens, '$';
    push @tokens, '$';
  }
  
  foreach my $n (1..$order) {
    getcounts(\@tokens, $counts[$n], $n, $order);
  }
}


foreach my $n (1..$order) {
  foreach my $key (sort keys %{$counts[$n]}) {
    print "$n\t$key\t$counts[$n]->{$key}\n";
  }
}


sub getcounts {
  my ($tokens, $count, $n, $order) = @_;
  my @tokens = @$tokens;
  for (my $i = $order-1; $i < @tokens; $i++) {
    my $key = join ' ', @tokens[$i-$n+1..$i];
    $count->{$key} = $count->{$key} ? $count->{$key}+1 : 1;
  }
}