#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long;

my $order = 3;
my $word = 0;
GetOptions(
  "o=i"  => \$order,
  "w"    => \$word,
)
|| die "CLI argument error!\n";

print "$order\n";

my @counts = ();
$counts[0] = 0; # unigram counts
foreach my $n (1..$order) {
  $counts[$n] = {}
}

while (<>) {
  chomp;
  my $s = $_;
  unless ($word) {
    $s =~ s/\s+/_/g;
    $s = join(' ', split(//,$s));
  }
  my @words = split /\s+/, $s;
  foreach my $n (1..$order-1) {
    unshift @words, '$';
    push @words, '$';
  }
  
  for (my $i = 0; $i < @words; $i++) {
    $counts[0]++;
    
    foreach my $n (1..$order) {
      next if $i-$n+1 < 0;
      my $ngram = join(' ',@words[$i-$n+1..$i]);
      #print scalar(@words),"\t$i\t$n\t$ngram\n";
      $counts[$n]->{$ngram} = $counts[$n]->{$ngram} ? $counts[$n]->{$ngram}+1 : 1;
    }
  }
}

my @lambda = ();
$lambda[0] = 0;
foreach my $n (1..$order) {
  $lambda[$n] = 1;
}

ORDERGRAM: foreach my $ordergram (keys %{$counts[$order]}) {
  my $count = $counts[$order]->{$ordergram}; # increment some lambda by this value.
  
  my @words = split /\s+/, $ordergram;
  my @s = (); # suffixes of $ordergram
  foreach my $n (1..$order) {
    $s[$n] = join(' ', @words[scalar(@words)-$n..scalar(@words)-1]);
  }
  
  foreach my $n (1..$order) {  
    if (!$counts[$n]->{$s[$n]}) {
      #print "No count: $n, $s[$n]\n";
      next ORDERGRAM;
    }
    if ($counts[$n]->{$s[$n]} < 2) {
      next ORDERGRAM;
    }
  }
  
  my @c = ();
  $c[0] = 0;
  $c[1] = ($counts[1]->{$s[1]}-1) / ($counts[0]-1);
  foreach my $n (2..$order) {
    $c[$n] = ($counts[$n]->{$s[$n]}-1) / ($counts[$n-1]->{$s[$n-1]}-1);
  }
  
  my $i = max(@c);
  $lambda[$i] += $count;
}

my $lsum = 0;
foreach my $n (1..$order) {
  $lsum += $lambda[$n];
}
foreach my $n (1..$order) {
  $lambda[$n] /= $lsum;
  print "$n\t$lambda[$n]\n";
}


sub max {
  my $max = $_[0];
  my $argmax = 0;
  for (my $i = 1; $i < @_; $i++) {
    if ($_[$i] > $max) {
      $max = $_[$i];
      $argmax = $i;
    }
  }
  return $argmax;
}
