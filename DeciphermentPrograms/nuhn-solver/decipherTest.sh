#!/bin/bash


files=Letters/encodedLetters/*

for file in $files
do
    #cat $file | python3 encodeMASC.py > encodedSamples/${file:12}.out
    #echo ${file:15}
    #cat $file | python3 split.py | perl sub-cipher-solver-nuhn_etal-2013.pl -c lmtrain_nyt_word.c3lm -d lmtrain_nyt_word.c3di -b 128 -w -i | python3 join.py
    cat $file | python3 split.py | perl sub-cipher-solver-nuhn_etal-2013.pl -c lm/letters/lmtrain_letters_word.c3lm -d lm/letters/lmtrain_letters_word.c3di -b 128 -w -i | python3 join.py  > Letters/decipheredLetters/${file:23}.dec
done