import re
import sys

if __name__ == "__main__":
    text = open(sys.argv[1]).read()
    print(re.sub(r'[^a-z ]', '', text.lower().replace("\n"," ")))