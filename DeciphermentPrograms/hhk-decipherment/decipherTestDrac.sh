#!/bin/bash


files=encodedSamples/D*

for file in $files
do
    #cat $file | python3 encodeMASC.py > encodedSamples/${file:12}.out
    #echo ${file:15}
    perl masc_solver_mcts.pl -n $file > decipherments/${file:15}.dec
done