import fileinput
import re

for line in fileinput.input():
    if line == "\n":
        continue
    print(re.sub("[^a-z ]", "", line.lower()).strip())