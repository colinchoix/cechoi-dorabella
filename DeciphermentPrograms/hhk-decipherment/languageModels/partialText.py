import fileinput

textLength = 143746
wordCount = 0
for line in fileinput.input():
    print(line, end = "")
    wordCount += len(line.split())
    if wordCount >= textLength:
        break