import fileinput
import re

for line in fileinput.input():
    if line == "\n":
        continue
    text = line.replace(" ", "_")
    print(" ".join(re.sub("[^a-z_]", "", text)))