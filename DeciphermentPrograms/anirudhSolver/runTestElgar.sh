#!/bin/bash


files=music24/ElgarSamples/formattedSamples/*

for file in $files
do
    python2 abram_search_english_to_english.py music24/ElgarSamples/Elgar24.arpa $file > music24/ElgarSamples/decipheredMusic/${file:38}
done

files=music24/ElgarSamples/decipheredMusic/*

for file in $files
do
    python3 music24/formatOutput.py $file > music24/ElgarSamples/decipherments/${file:37}
done