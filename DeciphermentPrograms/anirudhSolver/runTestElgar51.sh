#!/bin/bash


files=Music/Elgar/formattedSamples/*

for file in $files
do
    python2 colin_search_english_to_english.py Music/LM/Elgar.arpa $file > Music/Elgar/decipheredMusic/${file:29}
done

files=Music/Elgar/decipheredMusic/*

for file in $files
do
    python3 Music/formatOutput.py $file > Music/Elgar/decipherments/${file:28}
done