#!/bin/bash

files=encodedSamples/*

for file in $files
do
    python3 formatInput.py $file > formattedSamples/${file:15}
done