#!/bin/bash


files=Letters/formattedCiphers/*

for file in $files
do
    python2 abram_search_english_to_english.py LM/Letters.arpa  $file > Letters/decipheredLetters/${file:25}
done

files=Letters/decipheredLetters/*

for file in $files
do
    python3 Letters/formatOutput.py $file > Letters/decipherments/${file:26}
done