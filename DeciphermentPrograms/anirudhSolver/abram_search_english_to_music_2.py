import numpy as np
from random import shuffle,sample,randint
import kenlm
import os
import time
from nltk.tokenize import word_tokenize
from itertools import product
import txt_to_midi_fn
import subprocess
import argparse
from sys import exit

def encrypt(string):

    #somelist = ['x','z','k','q','j']
    somelist = ['x','q']
    indices = []
    for i,j in enumerate(string):
        if j in somelist:
            indices.append(i)

    string = filter(lambda x: x not in somelist,string)

    key1 = [chr(i) for i in range(ord('a'),ord('z')+1)]
    key1 = list(filter(lambda x: x not in somelist,key1))

    key2 = list(key1)
    shuffle(key2)

    enc_dict = dict(zip(key1,key2))
    enc_dict[' '] = ' '
    enc_str = []
    for i in string:
        enc_str.append(enc_dict[i])

    for i in indices:
        enc_str = insert(enc_str,'%',i)
    return ''.join(enc_str)


def eng2music(string):
    key1 = [chr(i) for i in range(ord('a'),ord('z')+1)]
    key1 = [item for item in key1 if item not in ['z','x','j','q','k']]

    #key1 = [item for item in key1 if item not in ['x','q']]

    list1 = [chr(i) for i in range(ord('A'),ord('G')+1)]
    list2 = [0.5,1.0,2.0]

    key2 = list(i + str(j) for i,j in product(list1,list2))
    for i in range(10):
        shuffle(key2)
    dec_dict = dict(zip(key1,key2))
    dec_dict[' '] = ' '
    dec_dict['%'] = '<unk>'
    dec = []
    for char in string:
        dec.append(dec_dict[char])
    return ' '.join(dec)


def decrypt(string, key1, key2):
    dec_dict = dict(zip(key1,key2))
    dec_dict[' '] = ' '
    dec_dict['<unk>'] = '<unk>'
    dec = []
    for char in string:
        dec.append(dec_dict[char])
    return ' '.join(dec)

def insert(list1,item,index):
    return(list1[:index]+[item]+list1[index:])

def score(string,model1):
    #return (0.5*model1.perplexity(string) + 0.5*model2.perplexity(string))
    return (-1*model1.score(string))

def swap(perm,n_mutations):

    number = randint(1,n_mutations)
    for i in range(number):
        int1,int2 = sample(list(np.arange(86)),2)
        perm[int1],perm[int2] = perm[int2],perm[int1]




list1 = [chr(i) for i in range(ord('A'),ord('G')+1)]
#list1.extend(['F#','G#','C#','E-','B-','A-','D#'])
#list1.append('F#')
#list1.append('C#')
#list1.append('E-')
#list1.append('B-')
list2 = [0.5,1.0,2.0]


corpus_tokens_vocab = list(i + str(j) for i,j in product(list1,list2))
our_tokens_vocab = list(i + str(j) for i,j in product(list1,list2))




parser = argparse.ArgumentParser()
parser.add_argument("Arpa",help = "Name of the arpa/binary file to be used")
parser.add_argument("Iter_Number",help = "Current Number in Iteration")

args = parser.parse_args()


if 'elgar' in args.Arpa:
    #model1 =kenlm.LanguageModel('Arpa_Files/elgar_small.binary')
    model1 =kenlm.LanguageModel('Arpa_Files/elgar_mix.binary')
    save_name = 'MxElgar'
elif 'bach' in args.Arpa:
    model1 =kenlm.LanguageModel('Arpa_Files/bach_train_music.binary')
    save_name = 'Bach'
else:
    print "Invalid Arpa"
    exit()


#model1 =kenlm.LanguageModel('midide.binary')

#model1 =kenlm.LanguageModel('total_corpus.binary')

#with open('split_text.txt','r') as f:
#    lines = f.readlines()

#i = randint(0,len(lines))
#string = lines[i]
#print("original string: %s" % string)
#string = string.replace('\n','')

# Dorabella string
string = 'a b c d e f g d h a i v w l v m v v f b b v n g o g n i p g v g f y d h r s c v v c f n w g v i v f t p w l y h h y i p c p f u p c l u u n p c v f u w p n d b n p f d l e d'


enc_str = string

#enc_str = encrypt(string)
print("enc_str",enc_str)
music_str = eng2music(enc_str)
final_str = music_str.split(' ')
final_str = list(filter(lambda x: x != '',final_str))

print(final_str)

iterations = 0

min_score = None
min_perm = None
best_dec = None

def abram_search():
    min_score = None
    min_perm = None
    n_mutations = 10
    best_score = None
    best_dec = None
    iterations = 0
    min_iter = 0

    start_time = time.time()
    while True:

        perm = list(corpus_tokens_vocab)
        shuffle(perm)
        min_score = None
        min_perm = list(perm)

        print(iterations)

        for j in range(0,20):
            for i in range(0,len(perm)):
                for v in corpus_tokens_vocab:
                    perm = list(min_perm)
                    if v in perm:
                        v_index = perm.index(v)
                        perm[v_index],perm[i] = perm[i],perm[v_index]
                    else:
                        perm[i] = v

                    dec_str = decrypt(final_str,our_tokens_vocab,perm)

                    cur_score = score(dec_str,model1)

                    if(min_score is None or cur_score<min_score):
                        min_score = cur_score
                        min_perm = list(perm)
                        min_iter = iterations
                        #print(min_score)

                    iterations+=1

        if(best_score is None or min_score<best_score):
            best_score = min_score
            best_perm = list(min_perm)
            best_dec = decrypt(final_str,our_tokens_vocab,best_perm)

        print(best_score)
        if(time.time() - start_time > 30):
            return best_dec,best_score
    return best_dec,best_score

if __name__ == "__main__":
    best_dec, best_score = abram_search()
    #print("best_dec,best_score",best_dec,best_score)
    print(best_score)

    with open('Results/'+save_name+'_dorabella_music.txt','a') as g:
        if(not(os.path.exists("Results/"+save_name+"_dorabella_scores.txt"))):
            with open("Results/"+save_name+"_dorabella_scores.txt",'w') as f:
                f.write(str(best_score)+'\n')
                g.write(best_dec+'\n')
        else:
            with open("Results/"+save_name+"_dorabella_scores.txt",'a') as f:
                f.write(str(best_score)+'\n')
                g.write(best_dec+'\n')
    if not(os.path.exists('Music/'+save_name+'/')):
        os.makedirs('Music/'+save_name+'/')

    mystream = txt_to_midi_fn.to_music(best_dec,best_score,save_name,args.Iter_Number)



    #txt_to_midi_fn.save_midi(mystream,'Music/'+save_name+'/'+str(best_score)+'_'+args.Iter_Number+'.mid')
