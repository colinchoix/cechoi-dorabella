#!/bin/bash


files=Music/Bach/formattedSamples/*

for file in $files
do
    python2 colin_search_english_to_english.py Music/LM/Bach.arpa $file > Music/Bach/decipheredMusic/${file:28}
done

files=Music/Bach/decipheredMusic/*

for file in $files
do
    python3 Music/formatOutput.py $file > Music/Bach/decipherments/${file:27}
done