#!/bin/bash

files=Bach/decipheredMusic/*

for file in $files
do
    python3 formatOutput.py $file > Bach/decipherments/${file:21}
done