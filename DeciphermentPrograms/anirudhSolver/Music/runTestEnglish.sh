#!/bin/bash

files=Elgar/encodedSamples/*

for file in $files
do
    python3 formatInput.py $file > Elgar/formattedSamples/${file:21}
done