import numpy as np
from random import shuffle,sample,randint,choice
import kenlm
#import util
import os
import time
import pickle
start_time = time.time()
from nltk.tokenize import word_tokenize
from itertools import product
from sys import exit

def encrypt(string):

    list1 = [chr(i) for i in range(ord('A'),ord('G')+1)]
    list1.extend(['F#','G#','C#','D#','F##'])
    list2 = [0.5,1.0,2.0]

    key1 = list(i + str(j) for i,j in product(list1,list2))
    key2 = list(i + str(j) for i,j in product(list1,list2))

    shuffle(key2)
    #key1 = ['M%03d' %i for i in range(21,107)]
    #key2 = ['M%03d' %i for i in range(21,107)]
    #shuffle(key2)

    enc_dict = dict(zip(key1,key2))

    enc_str = []
    for i in string:
        enc_str.append(enc_dict[i])

    return enc_str


def decrypt(string, key1, key2):
    dec_dict = dict(zip(key1,key2))
    dec_dict[' '] = ' '
    dec_dict['<unk>'] = '<unk>'
    dec = []
    for char in string:
        dec.append(dec_dict[char])
    return ' '.join(dec)

def score(string,model1):
    #return (0.5*model1.perplexity(string) + 0.5*model2.perplexity(string))
    return (-1*model1.score(string))


def insert(list1,item,index):
    return(list1[:index]+[item]+list1[index:])


args = parser.parse_args()


#corpus_tokens_vocab = ['M%03d' %i for i in range(21,108)]
#our_tokens_vocab = ['M%03d' %i for i in range(21,108)]

list1 = [chr(i) for i in range(ord('A'),ord('G')+1)]
list1.extend(['F#','G#','C#','D#','F##'])
list2 = [0.5,1.0,2.0]


corpus_tokens_vocab = list(i + str(j) for i,j in product(list1,list2))
our_tokens_vocab = list(i + str(j) for i,j in product(list1,list2))


# music 'language' model
model1 = kenlm.LanguageModel('Arpa_Files/bach_train_music.binary')

#model1 = kenlm.LanguageModel('total_corpus.binary')

#music = 'M058 M067 M063 M039 M051 M058 M057 M057 M058 M058 M039 M067 M063 M051 M055 M055 M067 M063 M055 M034 M046 M055 M058 M058 M046 M067 M063 M034 M057 M062 M068 M053 M041 M057 M058 M058 M060 M060 M053 M062 M068 M041 M061 M061 M062 M068 M034 M046 M062 M058 M058 M046 M034 M068 M067 M058 M063 M051 M039 M058 M057 M057 M058 M058 M051 M067 M063 M039 M055 M055 M067 M063 M055 M034 M046 M055 M058 M058 M046 M067 M063 M034 M062 M057 M068 M053 M041 M057 M058 M058 M060 M060 M053 M062 M068 M041 M061 M061 M062 M068 M034 M046 M062 M058 M058 M046 M034 M068 M063 M075 M051 M051'

with open('Test_Data/total_corpus_test_split.txt','r') as f:
#with open('test_music_split.txt','r') as f:
    lines = f.readlines()

test_line = choice(lines)
test_line = test_line.replace('\n','')

test_line = test_line.split(' ')
music = ' '.join(test_line)

print("Score of music is %s" %(score(music,model1)))
music = music.replace('\n','')
musicList = music.split(' ')
print("length",len(musicList))
#print(musicList)
flag = 0
indices = []
for i,j in enumerate(musicList):
    #if('#' in j or'-' in j):
    if('-' in j):
        #flag = 1
        indices.append(i)

#musicList = list(filter(lambda x: not('#' in x or '-' in x),musicList))
musicList = list(filter(lambda x: not('-' in x),musicList))
#print(musicList)
enc_str = encrypt(musicList)


for index in indices:
    enc_str = insert(enc_str,"<unk>",index)



print(enc_str)

iterations = 0

min_score = None
min_perm = None
best_dec = None

def abram_search():

    min_score = None
    min_perm = None
    n_mutations = 10
    best_score = None
    best_dec = None
    iterations = 0
    min_iter = 0

    start_time = time.time()
    while True:

        perm = list(corpus_tokens_vocab)
        shuffle(perm)
        min_score = None
        min_perm = list(perm)

        print(iterations)

        for j in range(0,20):
            for i in range(0,len(perm)):
                for v in corpus_tokens_vocab:
                    perm = list(min_perm)
                    if v in perm:
                        v_index = perm.index(v)
                        perm[v_index],perm[i] = perm[i],perm[v_index]
                    else:
                        perm[i] = v

                    dec_str = decrypt(enc_str,our_tokens_vocab,perm)

                    cur_score = score(dec_str,model1)

                    if(min_score is None or cur_score<min_score):
                        min_score = cur_score
                        min_perm = list(perm)
                        min_iter = iterations
                        #print(min_score)

                    iterations+=1

        if(best_score is None or min_score<best_score):
            print(best_score)
            best_score = min_score
            best_perm = list(min_perm)
            best_dec = decrypt(enc_str,our_tokens_vocab,best_perm)

        #print(best_score)
        if(time.time() - start_time > 30):
            return best_dec,best_score
    return best_dec,best_score

if __name__ == "__main__":
    #model2 = kenlm.LanguageModel('bach_music_o5.binary')
    best_dec, best_score = abram_search()
    #print("best_dec,best_score",best_dec,best_score)
    print(best_dec)
    print(best_score)
