import fileinput

for line in fileinput.input():
    if line == "\n":
        continue
    print(" ".join(list(line.replace(" ",""))), end = "")