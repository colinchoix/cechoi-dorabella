#!/bin/bash

files=decipheredLetters/*

for file in $files
do
    python3 formatOutput.py $file > decipherments/${file:18}
done