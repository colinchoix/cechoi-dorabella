#Encrypt music then convert to english

import numpy as np
from random import shuffle,sample,randint,choice
import kenlm
#import util
import os
import time
import pickle
start_time = time.time()
from nltk.tokenize import word_tokenize
import copy
from itertools import product



def encrypt(string):
    list1 = [chr(i) for i in range(ord('A'),ord('G')+1)]
    list2 = [0.5,1.0,2.0]

    key1 = list(i + str(j) for i,j in product(list1,list2))
    key2 = list(i + str(j) for i,j in product(list1,list2))

    shuffle(key2)

    enc_dict = dict(zip(key1,key2))

    enc_str = []
    for i in string:
        enc_str.append(enc_dict[i])

    return enc_str


def music2english(string):
    # Music notes
    list1 = [chr(i) for i in range(ord('A'),ord('G')+1)]
    #list1.append('F#')
    list2 = [0.5,1.0,2.0]
    key1 = list(i + str(j) for i,j in product(list1,list2))

    # English notes
    key2 = copy.deepcopy(key1)

    eng_list = list(np.arange(ord('a'),ord('z')+1))
    eng_list = [chr(i) for i in eng_list]
    shuffle(eng_list)

    key2 = list(eng_list[0:len(key1)])

    for shuffles in range(randint(1,10)):
        shuffle(key2)
    eng_dict = dict(zip(key1,key2))
    eng_dict['%'] = '%'
    eng_str = []
    for i in string:
        eng_str.append(eng_dict[i])
    return eng_str


def decrypt(string, key1, key2):
    dec_dict = dict(zip(key1,key2))
    dec_dict[' '] = ' '
    dec_dict['%'] = '%'
    dec = []
    for char in string:
        dec.append(dec_dict[char])
    return ' '.join(dec)

def score(string,model1):
    #return (0.5*model1.perplexity(string) + 0.5*model2.perplexity(string))
    return (-1*model1.score(string))

def swap(perm,n_mutations):

    number = randint(1,n_mutations)
    for i in range(number):
        int1,int2 = sample(np.arange(26),2)
        perm[int1],perm[int2] = perm[int2],perm[int1]
    return perm

def insert(list1,item,index):
    return(list1[:index]+[item]+list1[index:])


corpus_tokens_vocab = [chr(i+ord('a')) for i in list(np.arange(26))]
our_tokens_vocab = [chr(i+ord('a')) for i in list(np.arange(26))]


# character Trigram langauge model
#model1 = kenlm.LanguageModel('Arpa_Files/bible_french_clean_processed.binary')


parser = argparse.ArgumentParser()
parser.add_argument("Arpa",help = "Name of the arpa/binary file to be used")
parser.add_argument("Iter_Number",help = "Current Number in Iteration")

args = parser.parse_args()


if 'english' in args.Arpa:
    model1 =kenlm.LanguageModel('Arpa_Files/bible_english_clean_processed.binary')
    save_name = 'English'
elif 'latin' in args.Arpa:
    model1 =kenlm.LanguageModel('Arpa_Files/bible_vulgate_clean_processed.binary')
    save_name = 'Latin'
elif 'italian' in args.Arpa:
    model1 =kenlm.LanguageModel('Arpa_Files/bible_italian_clean_processed.binary')
    save_name = 'Italian'
elif 'french' in args.Arpa:
    model1 =kenlm.LanguageModel('Arpa_Files/bible_french_clean_processed.binary')
    save_name = 'French'
elif 'german' in args.Arpa:
    model1 =kenlm.LanguageModel('Arpa_Files/bible_german_clean_processed.binary')
    save_name = 'BibGerman'
elif 'freng' in args.Arpa:
    model1 =kenlm.LanguageModel('Arpa_Files/bible_french_english_combined.binary')
    save_name = 'Frenglish'
else:
    print "Invalid Arpa"
    exit()

#model1 = kenlm.LanguageModel('Arpa_Files/bible_italian_clean_processed.binary')


#with open('total_corpus_test_split.txt','r') as f:
with open('Test_Data/bach_test_music_split.txt','r') as f:
    lines = f.readlines()

test_line = choice(lines)
test_line = test_line.replace('\n','')

test_line = test_line.split(' ')
music = ' '.join(test_line[0:87])
musicList = music.split(' ')

indices = []
for i,j in enumerate(musicList):
    if('#' in j or '-' in j):
        indices.append(i)

musicList = list(filter(lambda x: not('#' in x or '-' in x),musicList))


enc_str = encrypt(musicList)
for index in indices:
    enc_str = insert(enc_str,"%",index) # Some unknown character

eng_str = music2english(enc_str)
eng_str = ''.join(eng_str)
print(eng_str)

min_score = None
min_perm = None
best_dec = None

def abram_search():

    min_score = None
    min_perm = None
    n_mutations = 10
    best_score = None
    best_dec = None
    iterations = 0

    min_iter = 0

    start_time = time.time()
    while True:

        perm = list(corpus_tokens_vocab)
        shuffle(perm)
        min_score = None
        min_perm = list(perm)

        print(iterations,perm)

        for j in range(0,20):
            for i in range(0,len(perm)):
                for v in corpus_tokens_vocab:
                    perm = list(min_perm)
                    if v in perm:
                        v_index = perm.index(v)
                        perm[v_index],perm[i] = perm[i],perm[v_index]
                    else:
                        perm[i] = v
                    #if(j>50):
                        #perm = swap(perm,n_mutations)


                    dec_str = decrypt(eng_str,our_tokens_vocab,perm)
                    cur_score = score(dec_str,model1)


                    if(min_score is None or cur_score<min_score):
                        min_score = cur_score
                        min_perm = list(perm)
                        min_iter = iterations

                    iterations+=1

        if(best_score is None or min_score<best_score):
            best_score = min_score
            best_perm = list(min_perm)
            best_dec = decrypt(eng_str,our_tokens_vocab,best_perm)

        print(best_dec,best_score)

        if(time.time() - start_time > 30):
            return best_dec,best_score
    return best_dec,best_score



if __name__ == "__main__":
    best_dec, best_score = abram_search()
    print("best_dec,best_score",best_dec,best_score)


    with open('Results/'+save_name+'_bach_decs.txt','a+') as g:
        with open("Results/'+save_name+'_bach_scores.txt",'a+') as f:
            f.write(str(best_score)+'\n')
            g.write(best_dec+'\n')
