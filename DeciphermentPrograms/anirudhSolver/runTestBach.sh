#!/bin/bash


files=music24/BachSamples/formattedSamples/*

for file in $files
do
    python2 abram_search_english_to_english.py music24/BachSamples/Bach24.arpa $file > music24/BachSamples/decipheredMusic/${file:37}
done

files=music24/BachSamples/decipheredMusic/*

for file in $files
do
    python3 music24/formatOutput.py $file > music24/BachSamples/decipherments/${file:36}
done