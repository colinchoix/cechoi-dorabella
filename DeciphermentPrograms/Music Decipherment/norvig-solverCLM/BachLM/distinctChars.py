import fileinput

for line in fileinput.input():
    text = line.split(" ")
    print(len(set(text)))

    count = {}
    for x in text:
        if x not in count:
            count[x] = 1
        else:
            count[x] += 1
    for x in sorted(count.items(), key = lambda x: x[1], reverse=True):
        print(x[0],"\t",x[1])
