#!/bin/bash


files=samples/*

for file in $files
do
    cat $file | python3 encodeMASC.py > encodedSamples/${file:8}.out
done