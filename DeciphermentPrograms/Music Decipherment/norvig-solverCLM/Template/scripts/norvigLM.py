import re
import sys
import itertools
import string

def getCharCombos(corpus,n):
    LETTERS = set(corpus)
    letterCombos = list(itertools.product(list(LETTERS), repeat=n))
    return {"".join(i) : 0 for i in letterCombos}

def count_2l(corpus):
    corpus = corpus.split()
    letterCombos = getCharCombos(corpus,2)

    for x in range(len(corpus)):
        if len(corpus[x:x+2]) != 2:
            continue
        letterCombos["".join(corpus[x:x+2])] += 1

    return sorted(letterCombos.items(), key = lambda x: x[1], reverse=True)

def count_3l(corpus):
    corpus = corpus.split()
    letterCombos = getCharCombos(corpus,3)

    for x in range(len(corpus)):
        if len(corpus[x:x+3]) != 3:
            continue
        letterCombos["".join(corpus[x:x+3])] += 1

    return sorted(letterCombos.items(), key = lambda x: x[1], reverse=True)


if __name__ == "__main__":
    corpus = open(sys.argv[1]).read()

    if sys.argv[2] == "2l":
        sortedCombos = count_2l(corpus)

        for x in sortedCombos:
            if x[1] != 0:
                print(x[0]+"\t"+str(x[1]))

    elif sys.argv[2] == "3l":
        sortedCombos = count_3l(corpus)

        for x in sortedCombos:
            if x[1] != 0:
                print(x[0]+"\t"+str(x[1]))

    elif sys.argv[2] == "a":

        c2l = open("count_2l.txt","w")
        sortedCombos = count_2l(corpus)
        for x in sortedCombos:
            if x[1] != 0:
                c2l.write(x[0]+"\t"+str(x[1])+"\n")

        c3l = open("count_3l.txt","w")
        sortedCombos = count_3l(corpus)
        for x in sortedCombos:
            if x[1] != 0:
                c3l.write(x[0]+"\t"+str(x[1])+"\n")