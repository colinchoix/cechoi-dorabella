#!/bin/bash

python2 norvigsolver.py dorabellaTest/dorabellaL1 0 EnglishLetterLM/count_2l.txt EnglishLetterLM/count_3l.txt > dorabellaTest/dorabella1.txt
python2 norvigsolver.py dorabellaTest/dorabellaL2 0 EnglishLetterLM/count_2l.txt EnglishLetterLM/count_3l.txt > dorabellaTest/dorabella2.txt
python2 norvigsolver.py dorabellaTest/dorabellaL3 0 EnglishLetterLM/count_2l.txt EnglishLetterLM/count_3l.txt > dorabellaTest/dorabella3.txt
python2 norvigsolver.py dorabellaTest/dorabellaL12 0 EnglishLetterLM/count_2l.txt EnglishLetterLM/count_3l.txt > dorabellaTest/dorabella12.txt
python2 norvigsolver.py dorabellaTest/dorabellaL13 0 EnglishLetterLM/count_2l.txt EnglishLetterLM/count_3l.txt > dorabellaTest/dorabella13.txt
python2 norvigsolver.py dorabellaTest/dorabellaL23 0 EnglishLetterLM/count_2l.txt EnglishLetterLM/count_3l.txt > dorabellaTest/dorabella23.txt
python2 norvigsolver.py dorabellaTest/dorabella.original 0 EnglishLetterLM/count_2l.txt EnglishLetterLM/count_3l.txt > dorabellaTest/dorabella.txt
