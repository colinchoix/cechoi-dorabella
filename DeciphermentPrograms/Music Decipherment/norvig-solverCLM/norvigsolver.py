from ngrams import *
import sys

for line in open(sys.argv[1]).read().split("\n"):
  line = line.rstrip()

  alphabet = None
  if sys.argv[2] == '0':
    alphabet = 'abcdefghijklmnopqrstuvwxyz'
  elif sys.argv[2] == '1':
    alphabet = 'abcdefghijklmnopqrstuvwxyABCDEFGHIJKLMNOPQRSTUVWXYZ'
  elif sys.argv[2] == '2':
    alphabet = 'abcdefghijklmABCDEFGHIJKLMNOPQRSTUVWXYZ' 
  elif sys.argv[2] == '3':
    alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWX'
    
  if len(sys.argv) > 3: 
    result = decode_subst_char(line, alphabet,sys.argv[3],sys.argv[4])
  else:
    result = decode_subst_char(line, alphabet)

  print result
