#!/bin/bash

rm -r LM
mkdir LM

for file in Texts/training/*.mid
do
    python3 scripts/music21_to_text_fn.py $file Texts/training/
done

for file in Texts/training/*.txt
do
    cat $file | tr '\n' ' ' >> LM/training.txt
done

python3 scripts/musicToCharAlt.py LM/training.txt > LM/training.chr
python3 scripts/norvigLM.py LM/training.chr a

mv count_2l.txt LM/
mv count_3l.txt LM/