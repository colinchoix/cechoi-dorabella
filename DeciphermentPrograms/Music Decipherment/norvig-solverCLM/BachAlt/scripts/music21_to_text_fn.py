from music21 import *
import os
from collections import defaultdict
import matplotlib.pyplot as plt
import argparse
import time

def quantize(num):
    if num<1:
        return 0.5
    elif num >1:
        return 2.0
    else :
        return 1.0


#filepath = "songs/"

#paths = os.listdir(filepath)


final_set = set()
count_dict = defaultdict(int)


def to_music(filename,folder):


    start_time = time.time()
    name = os.path.split(str(filename))[1]
    #score = corpus.parse(scores)
    #if(os.path.exists("corpus_final/"+name+".txt")):
    #    return 1

    #score = corpus.parse(filename)
    score = converter.parse(filename)

    k = score.analyze('key')
    print(k)

    if k.mode == "major":
        d = interval.Interval(k.tonic, pitch.Pitch('C'))
    if k.mode == "minor":
        d = interval.Interval(k.tonic, pitch.Pitch('a'))

    score = score.transpose(d)


    print(score.analyze('key'))

    num_parts = len(score.parts)

    for count in range(0,num_parts):
        #print(i)
        notes = score.parts[count].recurse().notes

        #try:
        #    txtnotes = ["%s%.3f" %(note.name,note.quarterLength) \
        #        if note.isNote else "%s%.3f" %(note.root().name,note.quarterLength) for note in notes] #for notes or chords

        try:
            txtnotes = ["%s%.1f" %(note.name,quantize(note.quarterLength)) \
                if note.isNote else "%s%.1f" %(note.root().name,quantize(note.quarterLength)) for note in notes] #for notes or chords


            #text_set = set(txtnotes)
            #final_set = final_set.union(text_set)

            #for x in txtnotes: count_dict[x] += 1


            if(not(os.path.exists(folder+"/"+name+".txt"))):
                with open(folder+"/"+name+".txt",'w') as f:
                    f.write(" ".join(txtnotes)+'\n')
            else:
                with open(folder+"/"+name+".txt",'a') as f:
                    f.write(" ".join(txtnotes)+'\n')



        except:
            print("oops")
    end_time = time.time()
    print(end_time- start_time)
    return 0





if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("filename",
                    help="file to be parsed")
    parser.add_argument("folder",
                    help="output folder name")
    args = parser.parse_args()


    if not(os.path.exists(args.folder+'/')):
        os.makedirs(args.folder+'/')

    x = to_music(args.filename,args.folder)
