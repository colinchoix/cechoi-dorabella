#!/bin/bash

rm -r Samples
mkdir Samples

for file in Texts/testing/*.mid
do
    python3 scripts/music21_to_text_fn.py $file Texts/testing/
done

for file in Texts/testing/*.txt
do
    python3 scripts/musicToCharAlt.py $file > Texts/testing/${file:14}.chr
done

mkdir Samples/samples

for file in Texts/testing/*.chr
do
    python3 scripts/randomTextSample.py $file 100 87 Samples/samples/
done

mkdir Samples/encodedSamples

for file in Samples/samples/*
do
    python3 scripts/encodeMASC.py $file > Samples/encodedSamples/${file:16}
done

mkdir Samples/decipherments