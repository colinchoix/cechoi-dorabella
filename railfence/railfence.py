import sys
import math

def enRail(text, n):
    rails = {}

    for c, x in enumerate(text):
        if c%n in rails:
            rails[c%n].append(x)
        else:
            rails[c%n] = [x]
        
    cipherText = ""

    for x in range(len(rails)):
        cipherText += "".join(rails[x])

    return cipherText

def splitRails(text, n):
    fullRails = len(text)%n
    fullRailLen = math.ceil(len(text)/n)

    rails = []

    for x in range(n):
        if x < fullRails or fullRails == 0:
            rails.append(text[:fullRailLen])
            text = text[fullRailLen:]
        else:
            rails.append(text[:fullRailLen-1])
            text = text[fullRailLen-1:]
    
    return rails

def deRail(text, n):
    rails = splitRails(text, n)

    decipherment = ""
    for x in range(len(text)):
        decipherment += rails[x%n][x//n]

    return decipherment

if __name__ == "__main__":
    text = open(sys.argv[1]).read()
    result = deRail(text.replace(" ",""), int(sys.argv[2]))
    print(result)
    #print(enRail(result,int(sys.argv[2])))

