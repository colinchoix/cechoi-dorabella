Took the dorabella and dorabella_alt transciptions and reversed a 2-41 rail fence transpostion cipher on them
Ran all the transcriptions through the norvig solver
Created a kenlm language model with the english letter corpus
Used kenlm to calculate perplexity of each decipherment and ranked the decipherments

The number in the name of the files is the number of rails used

dorabella_alt21.txt.dec for example is the dorabella_alt transciption deciphered with 21 rails