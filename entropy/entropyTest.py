import sys
import kenlm
import random
import re
import os
import math

def test(folder, LM):
    totalPerplexity = 0
    files = 0

    for file in os.listdir(folder):
        text = open(os.path.join(folder, file)).read()
        #print(LM.perplexity(" ".join(list(text))))

        totalPerplexity += LM.perplexity(" ".join(list(text)))
        files += 1

    print("Average Perplexity",totalPerplexity/files)
    print("Average Entropy",math.log(totalPerplexity/files,10))

if __name__ == "__main__":
    # Models
    bach = kenlm.LanguageModel("bach.arpa")
    bach24 = kenlm.LanguageModel("bach24.arpa")
    english = kenlm.LanguageModel("Frankenstein.arpa")

    # Texts
    bachText = open("bach.tra").read()
    bach24Text = open("bach24.tra").read()
    englishText = open("Frankenstein.tra").read()
    
    print("Bach",bach.perplexity(bachText))
    print("Bach24",bach.perplexity(bach24Text))
    print("English",english.perplexity(englishText))

    print("\nBach Bach")
    test("Samples/bachSamples", bach)
    print("\nElgar Bach")
    test("Samples/elgarSamples", bach)
    print("\nEnglish Bach")
    test("Samples/LetterSamples", bach)
    print("\nBach English")
    test("Samples/bachSamples", english)
    print("\nElgar English")
    test("Samples/elgarSamples", english)
    print("\nEnglish English")
    test("Samples/LetterSamples", english)

    print("--------------------------------------------------")

    print("\nBach24 Bach24")
    test("Samples/bach24Samples", bach24)
    print("\nElgar Bach24")
    test("Samples/elgar24Samples", bach24)
    print("\nEnglish Bach24")
    test("Samples/LetterSamples", bach24)
    print("\nBach24 English")
    test("Samples/bach24Samples", english)
    print("\nElgar English")
    test("Samples/elgar24Samples", english)
    print("\nEnglish English")
    test("Samples/LetterSamples", english)

    print("\ndorabella Bach24")
    test("Samples/dorabella/OG", bach)
    print("\ndorabella English")
    test("Samples/dorabella/OG", english)

    print("\ndorabellaDec Bach24")
    test("Samples/dorabella/DEC", bach)
    print("\ndorabellaDec English")
    test("Samples/dorabella/DEC", english)