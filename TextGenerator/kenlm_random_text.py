import sys
import kenlm
import numpy as np
import random

# bin/lmplz -o 5 --discount_fallback <dorabella_lines.txt >dorabellal.arpa
model = kenlm.LanguageModel(sys.argv[1])
print('{0}-gram model'.format(model.order))


def nextLetter(sentence):
    letters = np.array(['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X'])
    probability = []

    for x in letters:
        probability.append(10**model.score((sentence[-2:]+" "+x).strip()))

    probability = np.array(probability)
    probability /= probability.sum()

    return np.random.choice(letters, p=probability)

def generateArpaText(n):
    sentence = ""

    for x in range(n):
        sentence += nextLetter(sentence.strip()) + " "

    return sentence.strip()

def generateRandomText(n):
    letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X']
    sentence = ""
    
    for x in range(n):
        sentence += random.choice(letters) + " "

    return sentence.strip()

if __name__ == "__main__":
    #print(nextLetter("H E L L O W O R L D H O W A R E Y O U D O I N G"))
    #print(nextLetter("A B C D E F G D H A I J K"))
    #print(generateRandomText(87))
    
    sentence = generateArpaText(1000000)
    print(sentence)
    #print(model.score(sentence))
    #print(model.perplexity(sentence))