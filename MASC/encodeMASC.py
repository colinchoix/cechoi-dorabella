import fileinput
import random
import sys

def genRandKey(text):
    # Change letters if you want ot use different symbols

    letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
    #letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X']
    characters = set(text)
    key = {}

    random.shuffle(letters)
    for i, c in enumerate(characters):
        key[c] = letters[i] 

    # Spaces will remain encoded as spaces
    key[" "]=" "

    return key

if __name__ == "__main__":
    text = open(sys.argv[1]).read()
    key = genRandKey(text.replace(" ",""))

    for line in text.split("\n"):
        encodedText = "".join([key[i] for i in line])

        print(encodedText, end="")