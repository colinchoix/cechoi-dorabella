import sys

#mapping1 = {'A2': 'A', 'E3': 'B', 'B2': 'C', 'A3': 'D', 'A1': 'E', 'C2': 'F', 'G1': 'G', 'D1': 'H', 'H2': 'I', 'B3': 'J', 'F2': 'K', 'F1': 'L', 'B1': 'M', 'C3': 'N', 'H1': 'O', 'C1': 'P', 'F3': 'Q', 'D2': 'R', 'G2': 'S', 'G3': 'T', 'D3': 'U', 'E1': 'V', 'E2': 'W', 'H3': 'X', 'F1 B1': '1', 'B3 F3': '2', 'B2 F2': '3', 'F2 B2': '4', 'C2 G2': '5', 'H1 D1': '6', 'D1 H1': '7', 'A3 E3': '8'}
#mapping2 = {'A2': '⇐2', 'E3': '⇒3', 'B2': '⇖2', 'A3': '⇐3', 'A1': '⇐1', 'C2': '⇑2', 'G1': '⇓1', 'D1': '⇗1', 'H2': '⇙2', 'B3': '⇖3', 'F2': '⇘2', 'F1': '⇘1', 'B1': '⇖1', 'C3': '⇑3', 'H1': '⇙1', 'C1': '⇑1', 'F3': '⇘3', 'D2': '⇗2', 'G2': '⇓2', 'G3': '⇓3', 'D3': '⇗3', 'E1': '⇒1', 'E2': '⇒2', 'H3': '⇙3', 'F1 B1': '⇘1 ⇖1', 'B3 F3': '⇖3 ⇘3', 'B2 F2': '⇖2 ⇘2', 'F2 B2': '⇘2 ⇖2', 'C2 G2': '⇑2 ⇓2', 'H1 D1': '⇙1 ⇗1', 'D1 H1': '⇗1 ⇙1', 'A3 E3': '⇐3 ⇒3'}
#mapping3 = {'⇐2': 'A', '⇒3': 'B', '⇖2': 'C', '⇐3': 'D', '⇐1': 'E', '⇑2': 'F', '⇓1': 'G', '⇗1': 'H', '⇙2': 'I', '⇖3': 'J', '⇘2': 'K', '⇘1': 'L', '⇖1': 'M', '⇑3': 'N', '⇙1': 'O', '⇑1': 'P', '⇘3': 'Q', '⇗2': 'R', '⇓2': 'S', '⇓3': 'T', '⇗3': 'U', '⇒1': 'V', '⇒2': 'W', '⇙3': 'X', '⇘1 ⇖1': '1', '⇖3 ⇘3': '2', '⇖2 ⇘2': '3', '⇘2 ⇖2': '4', '⇑2 ⇓2': '5', '⇙1 ⇗1': '6', '⇗1 ⇙1': '7','⇐3 ⇒3': '8'}

mapping1 = {'A2': 'A', 'E3': 'B', 'B2': 'C', 'A3': 'D', 'A1': 'E', 'C2': 'F', 'G1': 'G', 'D1': 'H', 'H2': 'I', 'B3': 'J', 'F2': 'K', 'F1': 'L', 'B1': 'M', 'C3': 'N', 'H1': 'O', 'C1': 'P', 'F3': 'Q', 'D2': 'R', 'G2': 'S', 'G3': 'T', 'D3': 'U', 'E1': 'V', 'E2': 'W', 'H3': 'X', 'F1B1': '1', 'B3F3': '2', 'B2F2': '3', 'F2B2': '4', 'C2G2': '5', 'H1D1': '6', 'D1H1': '7', 'A3E3': '8'}
mapping2 = {'A2': '⇐2', 'E3': '⇒3', 'B2': '⇖2', 'A3': '⇐3', 'A1': '⇐1', 'C2': '⇑2', 'G1': '⇓1', 'D1': '⇗1', 'H2': '⇙2', 'B3': '⇖3', 'F2': '⇘2', 'F1': '⇘1', 'B1': '⇖1', 'C3': '⇑3', 'H1': '⇙1', 'C1': '⇑1', 'F3': '⇘3', 'D2': '⇗2', 'G2': '⇓2', 'G3': '⇓3', 'D3': '⇗3', 'E1': '⇒1', 'E2': '⇒2', 'H3': '⇙3', 'F1B1': '⇘1⇖1', 'B3F3': '⇖3⇘3', 'B2F2': '⇖2⇘2', 'F2B2': '⇘2⇖2', 'C2G2': '⇑2⇓2', 'H1D1': '⇙1⇗1', 'D1H1': '⇗1⇙1', 'A3E3': '⇐3⇒3'}
mapping3 = {'⇐2': 'A', '⇒3': 'B', '⇖2': 'C', '⇐3': 'D', '⇐1': 'E', '⇑2': 'F', '⇓1': 'G', '⇗1': 'H', '⇙2': 'I', '⇖3': 'J', '⇘2': 'K', '⇘1': 'L', '⇖1': 'M', '⇑3': 'N', '⇙1': 'O', '⇑1': 'P', '⇘3': 'Q', '⇗2': 'R', '⇓2': 'S', '⇓3': 'T', '⇗3': 'U', '⇒1': 'V', '⇒2': 'W', '⇙3': 'X', '⇘1⇖1': '1', '⇖3⇘3': '2', '⇖2⇘2': '3', '⇘2⇖2': '4', '⇑2⇓2': '5', '⇙1⇗1': '6', '⇗1⇙1': '7','⇐3⇒3': '8'}

def get_conversion(dict, character): 
    for k, v in dict.items(): 
        if character == v: 
            return k 
        elif character == k:
            return v

def convertTransctiption(text, mapping):
    '''
    Converts a text in list format to the desired mapping 
    mapping 1 = 'A2' mapping 2 = '⇐2' mapping 3 = 'A'
    '''
    convertedText = []

    for x in text:
        if mapping == 1:
            if x in mapping1.values():
                convertedText.append(get_conversion(mapping1, x))
            elif x in mapping2.values():
                convertedText.append(get_conversion(mapping2, x))
            else:
                convertedText.append(x)
        elif mapping == 2:
            if x in mapping2.keys():
                convertedText.append(get_conversion(mapping2, x))
            elif x in mapping3.values():
                convertedText.append(get_conversion(mapping3, x))
            else:
                convertedText.append(x)
        else:
            if x in mapping1.keys():
                convertedText.append(get_conversion(mapping1, x))
            elif x in mapping3.keys():
                convertedText.append(get_conversion(mapping3, x))
            else:
                convertedText.append(x)

    return convertedText

if __name__ == "__main__":
    # run python3 swapDecipherment.py dorabella.txt [1, 2, or 3 for mapping parameter]
    
    text = open(sys.argv[1]).read().split()
    result = convertTransctiption(text, int(sys.argv[2]))

    print(" ".join(result))
    

    

    
    