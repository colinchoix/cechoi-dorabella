import fileinput

freq = {}
totalChars = 0

for line in fileinput.input():
    line = line.split()
    for char in line:
        totalChars += 1
        if char in freq:
            freq[char] += 1
        else: 
            freq[char] = 1

freq = sorted(freq.items(), key = lambda x: x[1])

for x in freq:
    print(x[0],"\t",x[1],"\t", int(x[1])/totalChars)