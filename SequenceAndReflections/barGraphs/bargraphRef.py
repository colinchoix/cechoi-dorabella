import matplotlib.pyplot as plt
import numpy as np

# Average: 3.63509
x = np.arange(19)
frequency = [4710, 12230, 17364, 18744, 15976, 11969, 8090, 4988, 2893, 1569, 797, 378, 169, 65, 36, 14, 4, 3, 1]


fig, ax = plt.subplots()

ax.set_title('Number of Reflected Symbols')
ax.set_xlabel('Count')
ax.set_ylabel('Frequency')

plt.bar(x, frequency)
plt.xticks(x, ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '19'))
plt.show()