import matplotlib.pyplot as plt
import numpy as np

frequency = [133, 1244, 6563, 13249, 16732, 15674, 12949, 10407, 7199, 4710, 3344, 2516, 1515, 1248, 884, 499, 356, 216, 163, 136, 85, 64, 31, 33, 18, 14, 6, 2, 5, 2, 1, 1, 1]
x = np.arange(len(frequency))



fig, ax = plt.subplots()

ax.set_title('Longest Sequence Without Repeating Bumps (Music)')
ax.set_xlabel('Sequence Length')
ax.set_ylabel('Frequency')

plt.bar(x, frequency)
plt.xticks(x, [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 36])
plt.show()