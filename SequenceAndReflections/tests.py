from encodeDorabella import *
import re

def formatResults(results):
    frequency, labels = [], []
    results = sorted(list(results.items()), key=lambda kv: kv[0])
    '''
    for x in results:
        print("'",x[0],"', ",sep="", end="")
    print()
    for x in results:
        print(x[1],", ",sep="", end="")
    print()
    '''
    for x in results:
        frequency.append(x[1])
        labels.append(x[0])
    print(frequency)
    print(labels)
    return (frequency, labels)

def testOne(filepath,n,s):
    average = 0
    seqFrequency = {}

    for x in range(n):
        random.seed(x)

        key=['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
        random.shuffle(key)
        key = "".join(key)
        
        result = maxSequence(filepath, key, s)
        average += result[0]

        if result[0] in seqFrequency:
                seqFrequency[result[0]] += 1
        else:
            seqFrequency[result[0]] = 1

    
    print("Average:", average/n)
    
    return seqFrequency

def testTwo(filepath,n,s):
    average = 0
    refFrequency = {}

    for x in range(n):
        random.seed(x)

        key=['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
        random.shuffle(key)
        key = "".join(key)
        
        result = countReflections(filepath, key, s)
        average += result

        #print(result[0:3])
        if result in refFrequency:
                refFrequency[result] += 1
        else:
            refFrequency[result] = 1

    
    print("Average:", average/n)
    
    return refFrequency

def testThree(textPath, n, sampleLength):
    #musicText/beethoven_hammerklavier.txt
    #musicText/chopin_chpn_op25_e3.txt
    #elgar/Elgar_great_is_the_lord.txt
    text = open(textPath).read()
    average = 0
    refFrequency = {}

    for x in range(n):
        random.seed(x)
        dText = listToDorabella(text.split(" "))
        print(dText)
        result = countReflections(dText, sampleLength)
        average += result

        if result in refFrequency:
                refFrequency[result] += 1
        else:
            refFrequency[result] = 1

    print("Average:", average/n)
    
    return refFrequency

def testFour(textPath, n, sampleLength):
    text = open(textPath).read()
    text = list((re.sub(r'[^A-Z]', '', text.upper())))

    average = 0
    refFrequency = {}

    for x in range(n):
        random.seed(x)
        dText = listToDorabella(text)

        result = countReflections(dText, sampleLength)
        average += result

        if result in refFrequency:
                refFrequency[result] += 1
        else:
            refFrequency[result] = 1

    print("Average:", average/n)
    
    return refFrequency

def testFive(textPath, n, sampleLength):
    text = open(textPath).read()
    text = list((re.sub(r'[^A-Z]', '', text.upper())))

    average = 0
    refFrequency = {}

    for x in range(n):
        random.seed(x)
        dText = listToDorabella(text)

        result = countSequences(dText, sampleLength)
        longestSeq = max(result.keys())
        average += longestSeq

        if longestSeq in refFrequency:
                refFrequency[longestSeq] += 1
        else:
            refFrequency[longestSeq] = 1

    print("Average:", average/n)
    
    return refFrequency

def testSix(textPath, n, sampleLength):
    text = open(textPath).read()
    text = text.split(" ")

    average = 0
    refFrequency = {}

    for x in range(n):
        random.seed(x)
        dText = listToDorabella(text)

        result = countSequences(dText, sampleLength)
        longestSeq = max(result.keys())
        average += longestSeq

        if longestSeq in refFrequency:
                refFrequency[longestSeq] += 1
        else:
            refFrequency[longestSeq] = 1

    print("Average:", average/n)
    
    return refFrequency


if __name__ == "__main__":


    #result = testThree("elgar/Elgar_great_is_the_lord.txt", 100000, 87)

    '''
    result = testFive("englishText/The_Adventures_of_Sherlock_Holmes.txt", 100000, 87)
    formatResults(result)
    result = testFive("frenchText/letters_french.txt", 100000, 87)
    formatResults(result)
    '''
    result = testThree("dorabella_alternate2.txt", 100000, 74)
    formatResults(result)
    
