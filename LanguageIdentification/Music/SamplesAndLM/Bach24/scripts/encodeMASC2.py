import fileinput
import random

def genRandKey(text):
    '''
    Given a List of characters as input ["A", "AB", "B123", "AB"] return a random dorabella key 
    A key is a dictionary of the characters from the input list as the key and a mapping to a dorabella characters for the value
    '''

    letters = [
        #'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'
    ]
    characters = set(text)
    key = {}

    characters.remove(" ")

    random.shuffle(letters)
    for i, c in enumerate(characters):
        key[c] = letters[i] 

    key[" "] = " "

    return key

if __name__ == "__main__":
    text = open("EnglishCipher.tra").read()
    key = genRandKey(text.replace("\n", " "))
    
    for line in text.split("\n"):

        encodedText = "".join([key[i] for i in line])

        print(encodedText)