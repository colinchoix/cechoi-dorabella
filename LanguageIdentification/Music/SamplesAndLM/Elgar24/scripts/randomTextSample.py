import sys
import re
import random

samples = []

def getSample(wordList, sampleLen):

    for _ in range(100000):
        sample = random.randint(0,len(wordList)-sampleLen)

        # prevent duplicate samples
        if sample in samples:
            continue
        else:
            samples.append(sample)

        sampleText = ""

        for word in wordList[sample:]:
            sampleText += word

            if len(sampleText) == sampleLen:
                return sampleText
            elif len(sampleText) > sampleLen:
                break

    return False

if __name__ == "__main__":
    '''
    python3 randomTextSample.py [path of text] [number of samples to generate] [length of samples] [optionally add the output directory]
    python3 randomTextSample.py randomTextTest/test/Frankenstein.txt 10000 87 results/
    '''

    
    text = open(sys.argv[1]).read().replace("\n", "").split()
    
    for x in range(int(sys.argv[2])):
        result = getSample(text, int(sys.argv[3]))

        if not result: 
            break

        if len(sys.argv) > 4:
            filename = sys.argv[4]+sys.argv[1].split("/")[-1].split(".")[0]+str(x+1)+".sample"
        else:
            filename = sys.argv[1].split("/")[-1].split(".")[0]+str(x+1)+".sample"

        fh = open(filename, "w")
        fh.write(result)