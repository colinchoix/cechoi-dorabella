#!/bin/bash


files=resultsTSV/lmopt/Bach/*

for file in $files
do
    python3 scripts/getAvg.py $file 
done

files=resultsTSV/lmopt/Bach24/*

for file in $files
do
    python3 scripts/getAvg.py $file 
done

files=resultsTSV/lmopt/Elgar/*

for file in $files
do
    python3 scripts/getAvg.py $file 
done

files=resultsTSV/lmopt/Elgar24/*

for file in $files
do
    python3 scripts/getAvg.py $file 
done

files=resultsTSV/lmopt/English/*

for file in $files
do
    python3 scripts/getAvg.py $file 
done