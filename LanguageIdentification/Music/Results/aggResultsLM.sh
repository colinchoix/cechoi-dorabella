#!/bin/bash


files=lmopt/Bach/*
rm -r resultsTSV/lmopt/*
cp -r resultsTSV/temp/* resultsTSV/lmopt/

for file in $files
do
    python3 scripts/sortLMprob.py $file | python3 scripts/extractLMBach.py
done

files=lmopt/Bach24/*

for file in $files
do
    python3 scripts/sortLMprob.py $file | python3 scripts/extractLMBach24.py
done

files=lmopt/Elgar/*

for file in $files
do
    python3 scripts/sortLMprob.py $file | python3 scripts/extractLMElgar.py
done

files=lmopt/Elgar24/*

for file in $files
do
    python3 scripts/sortLMprob.py $file | python3 scripts/extractLMElgar24.py
done

files=lmopt/English/*

for file in $files
do
    python3 scripts/sortLMprob.py $file | python3 scripts/extractLMEnglish.py
done