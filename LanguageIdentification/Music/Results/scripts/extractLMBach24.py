import fileinput

path = "resultsTSV/lmopt/Bach24/"

for line in fileinput.input():
    line = line.split()
    if line[1] == "/Bach24.tra'":
        print(line)
        filename = path + "Bach24.txt"

        file_object = open(filename, 'a+')
        file_object.write(line[0]+"\t")
        file_object.close()

    elif line[1] == "/Bach.tra'":
        print(line)
        filename = path + "Bach.txt"

        file_object = open(filename, 'a+')
        file_object.write(line[0]+"\t")
        file_object.close()

    elif line[1] == "/Elgar24.tra'":
        print(line)
        filename = path + "Elgar24.txt"

        file_object = open(filename, 'a+')
        file_object.write(line[0]+"\t")
        file_object.close()

    elif line[1] == "/Elgar.tra'":
        print(line)
        filename = path + "Elgar.txt"

        file_object = open(filename, 'a+')
        file_object.write(line[0]+"\t")
        file_object.close()

    elif line[1] == "/udhr-eng.txt.tra":
        print(line)
        filename = path + "English.txt"

        file_object = open(filename, 'a+')
        file_object.write(line[0]+"\t")
        file_object.close()

    elif line[1] == "/EnglishEnciphere":
        print(line)
        filename = path + "EnglishEnciphered.txt"

        file_object = open(filename, 'a+')
        file_object.write(line[0]+"\t")
        file_object.close()