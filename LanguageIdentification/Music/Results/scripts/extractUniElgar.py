import fileinput

path = "resultsTSV/unigram/Elgar/"

for line in fileinput.input():
    if line[0] != "\t":
        continue

    line = line.strip().split("\t")
    
    if line[1] == "udhr-unicode/Bach24.tra":
        #print(line[0].split()[-1][:-1])
        filename = path + "Bach24.txt"

        file_object = open(filename, 'a+')
        file_object.write(line[0].split()[-1][:-1]+"\t")
        file_object.close()

    elif line[1] == "udhr-unicode/Bach.tra":
        print(line)
        filename = path + "Bach.txt"

        file_object = open(filename, 'a+')
        file_object.write(line[0].split()[-1][:-1]+"\t")
        file_object.close()

    elif line[1] == "udhr-unicode/Elgar24.tra":
        print(line)
        filename = path + "Elgar24.txt"

        file_object = open(filename, 'a+')
        file_object.write(line[0].split()[-1][:-1]+"\t")
        file_object.close()

    elif line[1] == "udhr-unicode/Elgar.tra":
        print(line)
        filename = path + "Elgar.txt"

        file_object = open(filename, 'a+')
        file_object.write(line[0].split()[-1][:-1]+"\t")
        file_object.close()

    elif line[1] == "eng":
        print(line)
        filename = path + "English.txt"

        file_object = open(filename, 'a+')
        file_object.write(line[0].split()[-1][:-1]+"\t")
        file_object.close()

    elif line[1] == "udhr-unicode/EnglishEnciphered.tra":
        print(line)
        filename = path + "EnglishEnciphered.txt"

        file_object = open(filename, 'a+')
        file_object.write(line[0].split()[-1][:-1]+"\t")
        file_object.close()
