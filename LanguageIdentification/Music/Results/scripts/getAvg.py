import fileinput

for line in fileinput.input():
    ranks = line.strip().split("\t")
    ranks = [int(x) for x in ranks][:100]

    print(fileinput.filename())
    print(sum(ranks)/len(ranks))
    #print(len(ranks))