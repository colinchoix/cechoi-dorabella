#!/bin/bash


files=resultsTSV/unigram/Bach/*

for file in $files
do
    python3 scripts/getAvg.py $file 
done

files=resultsTSV/unigram/Bach24/*

for file in $files
do
    python3 scripts/getAvg.py $file 
done

files=resultsTSV/unigram/Elgar/*

for file in $files
do
    python3 scripts/getAvg.py $file 
done

files=resultsTSV/unigram/Elgar24/*

for file in $files
do
    python3 scripts/getAvg.py $file 
done

files=resultsTSV/unigram/English/*

for file in $files
do
    python3 scripts/getAvg.py $file 
done