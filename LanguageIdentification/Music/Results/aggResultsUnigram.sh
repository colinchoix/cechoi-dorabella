#!/bin/bash


files=unigram/Bach/*
rm -r resultsTSV/unigram/*
cp -r resultsTSV/temp/* resultsTSV/unigram/

for file in $files
do
    python3 scripts/extractUniBach.py $file
done

files=unigram/Bach24/*

for file in $files
do
    python3 scripts/extractUniBach24.py $file
done

files=unigram/Elgar/*

for file in $files
do
    python3 scripts/extractUniElgar.py $file
done

files=unigram/Elgar24/*

for file in $files
do
    python3 scripts/extractUniElgar24.py $file
done

files=unigram/English/*

for file in $files
do
    python3 scripts/extractUniEnglish.py $file
done