#!/bin/bash


files=Music/TestSamples/Bach/*

cd ../..
for file in $files
do
    #18
    perl langid_probdist.pl -c $file -p 'udhr-unicode/*.tra' -d unigram > Music/Results/unigram/Bach/${file:23}
    perl langid_lmopt.pl -c $file -p 'udhr-unicode/*.tra' -r 20 -f 5 -b 10 > Music/Results/lmopt/Bach/${file:23}
done