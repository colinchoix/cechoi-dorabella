#!/bin/bash


files=Music/TestSamples/Elgar24/*

cd ../..
for file in $files
do
    #18
    perl langid_probdist.pl -c $file -p 'udhr-unicode/*.tra' -d unigram > Music/Results/unigram/Elgar24/${file:26}
    perl langid_lmopt.pl -c $file -p 'udhr-unicode/*.tra' -r 20 -f 5 -b 10 > Music/Results/lmopt/Elgar24/${file:26}
done