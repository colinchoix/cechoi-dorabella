#!/bin/bash


files=Music/TestSamples/English/*

cd ../..
for file in $files
do
    #18
    perl langid_probdist.pl -c $file -p 'udhr-unicode/*.tra' -d unigram > Music/Results/unigram/English/${file:26}
    perl langid_lmopt.pl -c $file -p 'udhr-unicode/*.tra' -r 20 -f 5 -b 10 > Music/Results/lmopt/English/${file:26}
done