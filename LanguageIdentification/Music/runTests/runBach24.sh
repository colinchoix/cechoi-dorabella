#!/bin/bash


files=Music/TestSamples/Bach24/*

cd ../..
for file in $files
do
    #18
    perl langid_probdist.pl -c $file -p 'udhr-unicode/*.tra' -d unigram > Music/Results/unigram/Bach24/${file:25}
    perl langid_lmopt.pl -c $file -p 'udhr-unicode/*.tra' -r 20 -f 5 -b 10 > Music/Results/lmopt/Bach24/${file:25}
done