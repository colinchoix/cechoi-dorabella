#!/bin/bash

files=decipherments/*
rm results*

for file in $files
do
    #echo ${temp##*/}
    python3 decAccuracy.py $file samples/${file:14} v >> results_v.txt
    python3 decAccuracy.py $file samples/${file:14} kv >> results_k.txt
done

    python3 aggResults.py results_v.txt
    python3 aggResults.py results_k.txt

