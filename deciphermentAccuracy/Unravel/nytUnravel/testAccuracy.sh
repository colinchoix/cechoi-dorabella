#!/bin/bash

files=deciphered/*
rm unravel_results*

for file in $files
do
    python3 decAccuracy.py $file nytSamples/${file:11:-4}.* v >> unravel_results_v.txt
    python3 decAccuracy.py $file nytSamples/${file:11:-4}.* kv >> unravel_results_k.txt
done

    python3 aggResults.py unravel_results_v.txt
    python3 aggResults.py unravel_results_k.txt

