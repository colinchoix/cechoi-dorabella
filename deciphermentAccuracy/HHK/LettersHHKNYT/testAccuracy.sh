#!/bin/bash

files=deciphered/*
rm results*

for file in $files
do
    python3 decAccuracy.py $file LetterSamples/${file:11:-8} v >> results_v.txt
    python3 decAccuracy.py $file LetterSamples/${file:11:-8} kv >> results_k.txt
done

    python3 aggResults.py results_v.txt
    python3 aggResults.py results_k.txt

