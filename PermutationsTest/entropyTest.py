import sys
import kenlm
import random
import re
import os
import math

def test(folder, LM):
    totalPerplexity = 0
    files = 0

    perplexity = []

    for file in os.listdir(folder):
        text = open(os.path.join(folder, file)).read()
        #print(file,"\t",LM.perplexity(" ".join(list(text))))

        perplexity.append([file,LM.score(" ".join(list(text)))])

        totalPerplexity += LM.score(" ".join(list(text)))
        files += 1

    #print("Average Perplexity",totalPerplexity/files)
    #print("Average Entropy",math.log(totalPerplexity/files,10))
    return perplexity

def test2(folder, LM):
    totalPerplexity = 0
    files = 0

    perplexity = []

    for file in os.listdir(folder):
        text = open(os.path.join(folder, file)).read()

        perplexity.append([file,LM.perplexity(" ".join(list(text)))])

        totalPerplexity += LM.perplexity(" ".join(list(text)))
        files += 1

    return perplexity

if __name__ == "__main__":
    # Models
    LM = kenlm.LanguageModel(sys.argv[1])

    if sys.argv[3] == "s":
        rank = test(sys.argv[2], LM)
        rank.sort(key = lambda x: x[1], reverse=True)
    elif sys.argv[3] == "p":
        rank = test2(sys.argv[2], LM)
        rank.sort(key = lambda x: x[1])
    

    for x in rank:
        print(x[0],"\t",x[1]) 
