#!/bin/bash

#Bach LM Score
python3 entropyTest.py Bach/bach.arpa Bach/musicPermutationsEnc/ s > Results/MusicMusicEnc_s.txt
python3 entropyTest.py Bach/bach.arpa Bach/musicPermutations/ s > Results/MusicMusicPlain_s.txt
python3 entropyTest.py Bach/bach.arpa EnglishLetters/englishPermutationsEnc/ s > Results/MusicEnglishEnc_s.txt
python3 entropyTest.py Bach/bach.arpa EnglishLetters/englishPermutations/ s > Results/MusicEnglish_s.txt
python3 entropyTest.py Bach/bach.arpa Dorabella/dorabellaPermutations/ s > Results/MusicDorabella_s.txt
python3 entropyTest.py Bach/bach.arpa Dorabella/dorabellaPermutationsEngDec/ s > Results/MusicDorabellaEng_s.txt
python3 entropyTest.py Bach/bach.arpa Dorabella/dorabellaPermutationsMusicDec/ s > Results/MusicDorabellaMusic_s.txt

#English LM Score
python3 entropyTest.py EnglishLetters/letters.arpa Bach/musicPermutationsEnc/ s > Results/EnglishMusicEnc_s.txt
python3 entropyTest.py EnglishLetters/letters.arpa Bach/musicPermutations/ s > Results/EnglishMusicPlain_s.txt
python3 entropyTest.py EnglishLetters/letters.arpa EnglishLetters/englishPermutationsEnc/ s > Results/EnglishEnglishEnc_s.txt
python3 entropyTest.py EnglishLetters/letters.arpa EnglishLetters/englishPermutations/ s > Results/EnglishEnglish_s.txt
python3 entropyTest.py EnglishLetters/letters.arpa Dorabella/dorabellaPermutations/ s > Results/EnglishDorabella_s.txt
python3 entropyTest.py EnglishLetters/letters.arpa Dorabella/dorabellaPermutationsEngDec/ s > Results/EnglishDorabellaEng_s.txt
python3 entropyTest.py EnglishLetters/letters.arpa Dorabella/dorabellaPermutationsMusicDec/ s > Results/EnglishDorabellaMusic_s.txt

