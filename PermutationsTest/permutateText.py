import sys
import random

text = list(open(sys.argv[1]).read().replace(" ",""))
perms = set()

while len(perms) < int(sys.argv[2]): 
     random.shuffle(text)
     perms.add(tuple(text))

for c, p in enumerate(perms,1):
    filename = sys.argv[3]+sys.argv[1].split("/")[-1] + str(c)
    f = open(filename, "w").write("".join(p))

# python3 permutateText.py dorabella.txt 1000 permutations/