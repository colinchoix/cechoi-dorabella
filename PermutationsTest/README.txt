1000 scrambles of multiple 87 length texts were scored to an english and a music language model with kenlm 

Enciphered Music - A 87 length section of a Bach piece enciphered with a substitution cipher
Plaintext Music - The same 87 length bach piece but not enciphered
Enciphered English - A 87 length sentence from Letters of Jane Austen enciphered with a substitution cipher
Plaintext English - The same 87 length sentence from Letters of Jane Austen but not enciphered
Dorabella - The dorabella cipher
Dorabella English Decipherment - A decipherment of the dorabella cipher from the norvig solver with the English Letters LM
Dorabella Music Decipherment - A decipherment of the dorabella cipher from the norvig solver with the Bach LM

Each of the above texts were randomly rearranged into 1000 different permutations (using random.shuffle function in python while ensuring no duplicates)
The 1000 permutations plus the original text were scored with kenlm on a English LM and a Music LM

The English LM used was trained on the English Letters Corpus
The Music LM was trained on a corpus of Bach Music
The 87 length texts were taken from a set seperate from the ones used to train the model

The results in the spreadsheet show what rank out of the 1001 permutation that the original unshuffled text ranked

	        BachLMScore	    EnglishLMScore
Dorabella	125/1001	    673/1001

Dorabella for example scored:
- 125 best out of the 1001 permutations when scored on a Music LM
- 673 best out of the 1001 permutations when scored on a Enlgish LM